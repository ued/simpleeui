// form select
var formSelect = $('.form-select');
if(formSelect.length) {

	var menu = $('.form-select-menu', formSelect),
		form = formSelect.parents('form');

	formSelect.hover(function(){
		$(this).addClass('form-select-open');
	}, function(){
		$(this).removeClass('form-select-open');
	});
	
	$('li', formSelect).hover(function(){
		$(this).addClass('hover');
	}, function(){
		$(this).removeClass('hover');
	});
	
	menu.delegate('li', 'click', function(){
		var li = $(this),
			action = li.attr('data-action') || '/root/',
			text = li.text(),
			placeholder = $('.form-select-selected', formSelect);
		
		placeholder.text(text);
		formSelect.removeClass('form-select-open');
		form.attr('action', action);
	});
	
}