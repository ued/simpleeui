$(function(){  

// highlight current page
!function(){
    var pathname = window.location.pathname,
        from = pathname.lastIndexOf('/') + 1,
        str = pathname.slice(from, -5),
        current = '#';
    switch(str){
        case 'index': current += 'index'; break;
        case 'css': current += 'css'; break;
        case 'javascript': current += 'javascript'; break;
        case 'getting-start': current += 'getting-start'; break;
    }
    $(current)[0] && $(current).addClass('current');
}();  


// # Modals
$('#myModal').on('shown', function(){
	//$('.modal-open .modal-backdrop')[0].style.height = $('html').height() + 'px';
})

// form select
var formSelect = $('.form-select');
if(formSelect.length) {

	var menu = $('.form-select-menu', formSelect),
		form = formSelect.parents('form');

	formSelect.hover(function(){
		$(this).addClass('form-select-open');
	}, function(){
		$(this).removeClass('form-select-open');
	});
	
	$('li', formSelect).hover(function(){
		$(this).addClass('hover');
	}, function(){
		$(this).removeClass('hover');
	});
	
	menu.delegate('li', 'click', function(){
		var li = $(this),
			action = li.attr('data-action') || '/root/',
			text = li.text(),
			placeholder = $('.form-select-selected', formSelect);
		
		placeholder.text(text);
		formSelect.removeClass('form-select-open');
		form.attr('action', action);
	});
	
}


// # Tips
$('#tips').length && $('#tips').popover();
